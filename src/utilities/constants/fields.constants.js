export const FIELD_TYPE = {
	1: "TYPE_STRING",
	2: "TYPE_TEXT",
	3: "TYPE_NUMBER",
	4: "TYPE_DROPLIST",
	5: "TYPE_CHECKBOX",
	6: "TYPE_RADIO",
	7: "TYPE_FILE",
	8: "TYPE_IMAGE",
	9: "TYPE_DATE_HIJRI",
	10: "TYPE_DATE",
	11: "TYPE_EMAIL",
};
