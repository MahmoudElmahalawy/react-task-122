export const TOKEN = process.env.REACT_APP_TOKEN;
export const API_URL = `${process.env.REACT_APP_API_URL}/data`;
export const HEADERS = {
	Accept: "application/json, text/plain, */*",
	Authorization: `Bearer ${TOKEN}`,
	"Access-Control-Allow-Origin": "*",
	"Content-Type": "application/json",
};
