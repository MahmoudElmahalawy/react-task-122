import { Row, Col, Form, Input, Upload, Checkbox, Select, DatePicker } from "antd";
import { PlusOutlined } from "@ant-design/icons";

import { FIELD_TYPE } from "../utilities/constants/fields.constants";

const CustomField = ({ type, id, name, label, is_mandatory, options }) => {
	const fieldProps = {
		key: id,
		name: name,
		label: label,
		rules: [{ required: is_mandatory ? true : false }],
	};
	const { Option } = Select;

	const onOptionChange = (value) => {
		// console.log(value);
	};

	const onCheckboxChange = (checkedValues) => {
		// console.log("checked = ", checkedValues);
	};

	const onDateChange = (date, dateString) => {
		console.log(date, dateString);
	};

	const normFile = (e) => {
		console.log("Upload event:", e);
		if (Array.isArray(e)) {
			return e;
		}
		return e && e.fileList;
	};

	const FIELD_MAP = {
		TYPE_STRING: (
			<Form.Item {...fieldProps}>
				<Input type="text" />
			</Form.Item>
		),
		TYPE_TEXT: (
			<Form.Item {...fieldProps}>
				<Input type="text" />
			</Form.Item>
		),
		TYPE_NUMBER: (
			<Form.Item {...fieldProps}>
				<Input type="number" />
			</Form.Item>
		),
		TYPE_DROPLIST: (
			<Form.Item {...fieldProps}>
				<Select placeholder="Select an option" onChange={onOptionChange} allowClear>
					{options?.map((opt) => (
						<Option key={opt.id} value={opt.name}>
							{opt.name}
						</Option>
					))}
				</Select>
			</Form.Item>
		),
		TYPE_CHECKBOX: (
			<Form.Item {...fieldProps}>
				<Checkbox.Group style={{ width: "100%" }} onChange={onCheckboxChange}>
					<Row>
						{options?.map((opt) => (
							<Col key={opt.id} span={8}>
								<Checkbox value={opt.name}>{opt.name}</Checkbox>
							</Col>
						))}
					</Row>
				</Checkbox.Group>
			</Form.Item>
		),
		TYPE_RADIO: "",
		TYPE_FILE: (
			<Form.Item {...fieldProps} valuePropName="fileList" getValueFromEvent={normFile}>
				<Upload accept="image/*" maxCount={1} listType="picture-card" beforeUpload={() => false}>
					<div>
						<PlusOutlined />
						<div style={{ marginTop: 8 }}>Upload</div>
					</div>
				</Upload>
			</Form.Item>
		),
		TYPE_IMAGE: (
			<Form.Item {...fieldProps} valuePropName="fileList" getValueFromEvent={normFile}>
				<Upload accept="image/*" maxCount={1} listType="picture-card" beforeUpload={() => false}>
					<div>
						<PlusOutlined />
						<div style={{ marginTop: 8 }}>Upload</div>
					</div>
				</Upload>
			</Form.Item>
		),
		TYPE_DATE_HIJRI: "",
		TYPE_DATE: (
			<Form.Item {...fieldProps}>
				<DatePicker onChange={onDateChange} style={{ width: "100%" }} />
			</Form.Item>
		),
		TYPE_EMAIL: (
			<Form.Item {...fieldProps}>
				<Input type="email" />
			</Form.Item>
		),
	};

	return FIELD_MAP[FIELD_TYPE[type]];
};

export default CustomField;
