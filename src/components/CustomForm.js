import { useEffect, useState } from "react";

import { Card, Form, Button } from "antd";

import CustomField from "./CustomField";

import axios from "axios";

import { API_URL, HEADERS } from "../utilities/constants/api.constants";

const layout = {
	labelCol: { span: 8 },
	wrapperCol: { span: 16 },
};

const tailLayout = {
	wrapperCol: { offset: 8, span: 16 },
};

const CustomForm = ({ id, title, fields }) => {
	const [formFields, setFormFields] = useState(null);

	const [form] = Form.useForm();

	const onFinish = (values) => {
		const formData = new FormData();
		const postData = {
			sample_id: id,
			fields: Object.keys(values).map((field) => ({ field_id: field, answer: values[field] })),
		};
		console.log(postData);

		for (const name in postData) {
			formData.append(name, postData[name]);
		}

		// axios.post(API_URL, formData, { HEADERS });
	};

	const onReset = () => {
		form.resetFields();
	};

	useEffect(() => {
		setFormFields(fields);
	}, [fields]);

	return formFields === null ? null : (
		<Card title={title} style={{ width: 600 }}>
			<Form {...layout} form={form} name="control-hooks" onFinish={onFinish}>
				{formFields.map((field) => (
					<CustomField
						key={field.id}
						type={[field.type]}
						id={field.id}
						name={field.id}
						label={field.name}
						is_mandatory={field.is_mandatory}
						options={field.options}
					/>
				))}
				<Form.Item {...tailLayout}>
					<Button type="primary" htmlType="submit">
						Submit
					</Button>
					<Button htmlType="button" onClick={onReset}>
						Reset
					</Button>
				</Form.Item>
			</Form>
		</Card>
	);
};

export default CustomForm;
