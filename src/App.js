import { useState, useEffect } from "react";

import { Row, Col, Space } from "antd";

import CustomForm from "./components/CustomForm";

import axios from "axios";

import { API_URL, HEADERS } from "./utilities/constants/api.constants";

const App = () => {
	const [data, setData] = useState(null);

	useEffect(async () => {
		try {
			const response = await axios.get(API_URL, { HEADERS });
			setData(response.data);
		} catch (err) {
			console.error(err);
		}
	}, []);

	return (
		<Row justify="center">
			<Col xs={12} sm={12} md={6}>
				<Space direction="vertical">
					{data === null
						? null
						: data.map((e) => <CustomForm key={e.id} id={e.id} title={e.name} fields={e.fields} />)}
				</Space>
			</Col>
		</Row>
	);
};

export default App;
